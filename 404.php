<?php get_header(); ?>

	<main class="max-width">
		
		<h1 class="page-title">Oops!</h1>

		<h2 class="section-heading">
			Page not found.
		</h2>

		<p>
			Why not try searching for what you're looking for?
		</p>

		<?php get_search_form(); ?>

	</main>

	<script type="text/javascript">
		(function($){

			//Focus on the Search Field after it has loaded
			$('#s').focus();

		})( jQuery );
	</script>

<?php get_footer(); ?>