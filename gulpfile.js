

/**
*
*   Gulp (v4) File - Javascript Management
*
*   - This file handles the Javascript & SCSS Compiling & Minification
*
**/
var path            = require('path');
var webpack         = require('webpack');
var gulp            = require('gulp');
var file            = require('gulp-file');
var babel           = require('gulp-babel');
var gulpWebpack     = require('gulp-webpack');
var concat          = require('gulp-concat');
var uglify          = require('gulp-uglify');
var watch           = require('gulp-watch');
var plumber         = require('gulp-plumber');
var postcss         = require('gulp-postcss');
var autoprefixer    = require('autoprefixer');
var sass            = require('gulp-sass');
var glob            = require('gulp-sass-bulk-import');
var tinypng         = require('gulp-tinypng-compress');
var browserSync     = require('browser-sync').create();




    /**
    *
    *   TASK: styles
    *       - This compiles all of the css into sass/scss
    *
    **/
    gulp.task('styles', function(){

        for( var i=0, files=[ 'editor', 'login' , 'main' , 'typography' ]; i < files.length; i++ ){

            //Compile Main Styles
            return gulp.src('scss/' + files[i] + '.scss')
                .pipe(plumber())
                .pipe(glob())
                .pipe(sass({ outputStyle: 'compressed' }).on('error' , sass.logError ) )
                .pipe(postcss([ autoprefixer ]))
                .pipe(gulp.dest('css/'))
                .pipe(browserSync.stream());

        }

    });




    /**
    *
    *   TASK: Libraries
    *       - This task is meant to compile and minify js libraries downloaded through bower
    *
    **/
    gulp.task('libraries', function(){

        //JS Libraries
        var files = [
            // Include the bower distribution files here
            // 'js/libraries/....
        ];

        //Compile Javascript
        if( files.length > 0 )
            return gulp.src(files)
                .pipe(plumber())
                .pipe(concat( 'libraries.js' ))
                .pipe(uglify())
                .pipe(gulp.dest('js/'));

    });




    /**
    *
    *   TASK: scripts
    *       - This compiles all of the Javascript (ES6, React, etc.)
    *       - This uses Babel & Webpack to compile
    *
    **/
    gulp.task('scripts', function(){

        //JS Libraries
        var files = [
            'js/src/**/*.js'
        ];

        //Compile Javascript
        return gulp.src(files)
            .pipe(plumber())
            .pipe(gulpWebpack({
                mode:   'production',
                entry:  path.resolve(__dirname,'js/src'),
                output: {
                    filename:   'script.js',
                    publicPath: path.resolve(__dirname,'js')
                },
                module: {
                    rules:[{
                        test:       /\.(js|jsx)$/,
                        loader:     'babel-loader',
                        options:    {
                            presets: [ '@babel/preset-env', '@babel/preset-react' ]
                        }
                    }]
                }
            },webpack))
            .pipe(concat( 'scripts.js' ))
            .pipe(uglify())
            .pipe(gulp.dest('js/'));
    });




    /**
    *
    *   TASK: images
    *       - This uses TinyPNG Compressor to compress images on the fly
    *
    **/
    gulp.task('images', function(){

        return gulp.src( 'images/src/**/*.{png,jpg,jpeg}' )
            .pipe(plumber())
            .pipe(tinypng({
                key:        'FXMbEM5RYjXyam0Kc8KbTxU1RAQ36ybo',
                sigFile:    'images/.tinypng-sigs',
                log:        true
            }))
            .pipe(gulp.dest( 'images/min/' ))

    });




    /**
    *
    *   TASK: watch
    *       - This handles the watch function for all of the tasks
    *
    **/
    gulp.task('watch', function(){

        browserSync.init({
            // port: 3000,
            proxy: 'http://example.com/',
            reloadOnRestart: true,
        });

        gulp.watch('js/libraries/**/*.js', gulp.series('libraries') );
        gulp.watch('js/src/**/*.js', gulp.series('scripts') );
        gulp.watch('scss/**/*.scss', gulp.series('styles') );
        gulp.watch(['**/**/*.php', '**/**/*.html']).on('change',browserSync.reload);

    });





    /**
    *
    *   TASK: default
    *       - This kicks the script off and loads all previous tasks
    *
    **/
    gulp.task( 'default' , gulp.parallel('watch', 'styles', 'scripts', 'libraries', 'images') );





